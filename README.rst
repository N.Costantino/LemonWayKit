=====
LemonWayKit
=====

LemonWayKit: Python API to access Lemonway web services

Detailed documentation is in the "docs" directory.

Installation
============

Install it with ``pip``:

	pip install git+https://gitlab.com/N.Costantino/LemonWayKit.git